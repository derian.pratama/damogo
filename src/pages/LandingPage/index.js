import {
  Blog,
  DemoAplikasi,
  Footer,
  Header,
  Hero,
  Kerjasama,
  Restoran,
  Supplier,
} from "components";
import React, { Component } from "react";

export default class LandingPage extends Component {
  render() {
    return (
      <>
        <Header {...this.props} />
        <Hero />
        <DemoAplikasi />
        <Blog />
        <Kerjasama />
        <Restoran />
        <Supplier />
        <Footer />
      </>
    );
  }
}
