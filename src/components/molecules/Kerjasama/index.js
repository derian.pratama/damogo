import { ImgAlfa, ImgPenyetan, ImgPlaza, ImgRoyal } from "assets";
import React from "react";
import "./Kerjasama.scss";

const Kerjasama = () => {
  return (
    <section className="kerjasama container">
      <div className="text-center mb-5">
        <h5>DAMOGO</h5>
        <h4>Kerjasama</h4>
      </div>
      <div className="row">
        <div className="col-md-3 d-flex flex-row justify-content-around align-items-center">
          <img src={ImgAlfa} alt="loyal" height="100" />
        </div>
        <div className="col-md-3 d-flex flex-row justify-content-around align-items-center">
          <img src={ImgRoyal} alt="loyal" height="100" />
        </div>
        <div className="col-md-3 d-flex flex-row justify-content-around align-items-center">
          <img src={ImgPlaza} alt="loyal" height="100" />
        </div>
        <div className="col-md-3 d-flex flex-row justify-content-around align-items-center">
          <img src={ImgPenyetan} alt="loyal" height="100" />
        </div>
      </div>
    </section>
  );
};

export default Kerjasama;
