import Header from "./Header";
import Hero from "./Hero";
import DemoAplikasi from "./DemoAplikasi";
import Blog from "./Blog";
import Kerjasama from "./Kerjasama";
import Restoran from "./Restoran";
import Supplier from "./Supplier";
import Footer from "./Footer";

export {
  Header,
  Hero,
  DemoAplikasi,
  Blog,
  Kerjasama,
  Restoran,
  Supplier,
  Footer,
};
