import { ImgRestoran } from "assets";
import { Button } from "components/atoms";
import React from "react";
import "./Restoran.scss";

const Restoran = () => {
  return (
    <section className="restoran container-fluid">
      <div className="row px-5">
        <div className="col">
          <img src={ImgRestoran} alt="restoran" className="img-fluid" />
        </div>
        <div className="col-md-6 mb-5">
          <h5>DAMOGO</h5>
          <h4>Restoran</h4>
          <p>
            Aplikasi gratis kami adalah cara paling efisien untuk memesan dari
            semua pemasok Anda. Selesaikan pesanan lebih cepat, tidur lebih
            awal.
          </p>
          <Button className="btn px-5 rounded-pill mr-4 btn-info" hasShadow>
            Pelajari selengkapnya
          </Button>
        </div>
      </div>
    </section>
  );
};

export default Restoran;
