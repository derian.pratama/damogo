import { HeroImage, ICWa } from "assets";
import { Button } from "components/atoms";
import React from "react";
import "./Hero.scss";

const Hero = () => {
  return (
    <section className="hero container-fluid">
      <div className="row align-items-center ">
        <div className="col-md-6">
          <div className="container pl-5 mt-5 mb-3">
            <h1 className="font-weight-bold line-height-2 mb-3">
              Create taste,
              <br /> not <span>waste</span>
            </h1>
            <p className="mb-5" style={{ lineHeight: "25.48px", fontSize: 18 }}>
              Selamatkan makanan lezat, makanan tidak terjual dari restaurant
              favoritmu, toko, dan pertanian agar tidak terbuang sia-sia serta
              dapatkan harga hemat hingga 90% dari harga reguler!
            </p>
            <Button className="btn px-5 rounded-pill mr-4" hasShadow isPrimary>
              Buat janji temu!
            </Button>
            <Button
              className="btn px-5 rounded-pill"
              hasShadow
              isOutlinePrimary
            >
              Bertemu tim kami
            </Button>
          </div>
        </div>
        <div className="col">
          <img src={HeroImage} alt="blog" className="img-fluid" />
          <a href="/" className="position-absolute">
            <img src={ICWa} alt="WA Icon" />
          </a>
        </div>
      </div>
    </section>
  );
};

export default Hero;
