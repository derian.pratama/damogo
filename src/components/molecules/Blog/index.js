import { ImgBlog } from "assets";
import { Button } from "components/atoms";
import React from "react";
import "./Blog.scss";

const Blog = () => {
  return (
    <section className="blog container-fluid ">
      <div className="row align-items-center">
        <div className="col-md-7">
          <div className="container pl-5 mt-3 mb-3">
            <div className="mb-5">
              <h5>BLOG</h5>
              <h4>Cara menyimpan daging agar awet dan segar tanpa Freezer </h4>
            </div>
            <p>
              Sudah banyak orang yang tahu jika menyimpan daging dalam freezer
              menjadi cara mudah menyimpan stok daging supaya awet dan tahan
              lama. Tapi bagaimana ya jika tidak memiliki kulkas atau freezer?
              Tenang! Ada kok caranya,
              <br /> <span>yuk disimak!</span>
            </p>
            <Button
              className="btn px-5 rounded-pill mr-4 btn-secondary text-white mt-4"
              hasShadow
            >
              Baca selengkapnya
            </Button>
          </div>
        </div>
        <div className="col" style={{ paddingRight: 0 }}>
          <img src={ImgBlog} alt="blog" className="img-fluid" />
        </div>
      </div>
    </section>
  );
};

export default Blog;
