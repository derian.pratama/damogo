import { Logo } from "assets";
import { Button } from "components";
import React from "react";

const Header = (props) => {
  const getNavLinkClass = (path) => {
    return props.location.pathname === path ? " active" : "";
  };
  return (
    <header className="spacing-sm px-5" style={{ zIndex: 1 }}>
      <div className="container-fluid">
        <nav className="navbar navbar-expand-lg">
          <img src={Logo} alt="logo" width="110" />
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav ml-auto">
              <li className={`nav-item${getNavLinkClass("/")}`}>
                <div style={{ position: "relative" }}>
                  <Button className="nav-link" type="link" href="/">
                    Beranda
                  </Button>
                  <div className="pill" />
                </div>
              </li>
              <li className={`nav-item${getNavLinkClass("/browse-by")}`}>
                <div style={{ position: "relative" }}>
                  <Button className="nav-link" href="/browse-by" type="link">
                    Restoran
                  </Button>
                  <div className="pill" />
                </div>
              </li>
              <li className={`nav-item${getNavLinkClass("/stories")}`}>
                <div style={{ position: "relative" }}>
                  <Button className="nav-link" href="/stories" type="link">
                    Supplier
                  </Button>
                  <div className="pill" />
                </div>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Header;
