import { ICPlay, ImgDemoAplikasi } from "assets";
import { Button } from "components/atoms";
import React from "react";
import "./DemoAplikasi.scss";

const DemoAplikasi = () => {
  return (
    <section className="demo-aplikasi container-fluid">
      <div className="text-center mb-5">
        <h5>DAMOGO</h5>
        <h4>Demo Aplikasi</h4>
      </div>
      <div className="container-fluid px-5">
        <div className="row align-items-center">
          <div className="col-md-4">
            <img
              src={ImgDemoAplikasi}
              alt="demo aplikasi"
              className="img-fluid"
            />
          </div>
          <div className="col">
            <p>
              <span>DamoGo</span> adalah sistem yang membantu proses pengadaan
              bahan makanan lebih mudah dan efisien!
            </p>
            <p>
              Atur semua pesanan dari supplier maupun bahan dari kamu ke
              franchise secara online
            </p>
            <Button className="btn px-5 rounded-pill mr-4" hasShadow isPrimary>
              Lihat Demo <img src={ICPlay} alt="ic play" className="ml-2" />
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DemoAplikasi;
