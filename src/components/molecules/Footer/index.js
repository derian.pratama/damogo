import { ImgAndroid, ImgIos, Logo } from "assets";
import React from "react";
import "./Footer.scss";

const Footer = () => {
  return (
    <section className="footer container-fluid">
      <div className="px-5">
        <img src={Logo} alt="logo" height="25" />
        <div className="row">
          <div className="col-md-3">
            <h4>Ikuti Kami</h4>
            <ul>
              <li>
                <a href="/">Terms of Use</a>
              </li>
              <li>
                <a href="/">Privacy</a>
              </li>
              <li>
                <a href="/">Careers</a>
              </li>
              <li>
                <a href="/">About</a>
              </li>
              <li>
                <a href="/">CA Supply Chains Act</a>
              </li>
              <li>
                <a href="/">Sustainability</a>
              </li>
              <li>
                <a href="/">Affiliates</a>
              </li>
              <li>
                <a href="/">Recall Info</a>
              </li>
              <li>
                <a href="/">Inclusion and Diversity</a>
              </li>
            </ul>
          </div>
          <div className="col-md-3">
            <h4>Hubungi kami</h4>
            <h5>ALamat</h5>
            <p>
              Jl. Prof. Herman Yohanes No.1212, Terban, Kec. Gondokusuman, Kota
              Yogyakarta, Daerah Istimewa Yogyakarta 55223
            </p>
            <h5>Jam buka</h5>
            <p>Mon - Fri 6:00 am - 8:00 pm Sat & Sun 9:30 am - 6:00 pm</p>
          </div>
          <div className="col d-flex justify-content-end">
            <div>
              <h4>Download DamoGO App</h4>
              <div style={{ marginTop: 37 }}>
                <a href="/">
                  <img
                    src={ImgAndroid}
                    alt="android"
                    height="48"
                    style={{ marginRight: 24 }}
                  />
                </a>
                <a href="/">
                  <img src={ImgIos} alt="ios" height="48" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;
