import { ImgSupplier } from "assets";
import { Button } from "components/atoms";
import React from "react";
import "./Supplier.scss";

const Supplier = () => {
  return (
    <section className="supplier container-fluid">
      <div className="row px-5">
        <div className="col-md-7 mb-5">
          <h5>DAMOGO</h5>
          <h4>Untuk Supplier</h4>
          <p>
            Baik Anda adalah pertanian milik keluarga atau distributor nasional,
            platform khusus kami akan mengubah cara Anda berbisnis. Hemat waktu
            untuk tugas manual, kurangi kesalahan pesanan, dan jual lebih banyak
            produk. .
          </p>
          <Button className="btn px-5 rounded-pill mr-4 btn-success" hasShadow>
            Pelajari selengkapnya
          </Button>
        </div>
        <div className="col">
          <img src={ImgSupplier} alt="supplier" className="img-fluid" />
        </div>
      </div>
    </section>
  );
};

export default Supplier;
