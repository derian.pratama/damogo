import Logo from "./damogo_logo.png";
import HeroImage from "./hero_image.png";
import ImgDemoAplikasi from "./demo_aplikasi.png";
import ImgBlog from "./blog.png";
import ImgRoyal from "./LogoRoyal AMBARUKMO-01.png";
import ImgPlaza from "./Plaza_Ambarrukmo.png";
import ImgAlfa from "./Logo_Alfamidi.png";
import ImgPenyetan from "./penyetan.jpg";
import ImgRestoran from "./restoran.png";
import ImgSupplier from "./supplier.png";
import ImgAndroid from "./google-play-1.png";
import ImgIos from "./app-store.png";

export {
  Logo,
  HeroImage,
  ImgDemoAplikasi,
  ImgBlog,
  ImgRoyal,
  ImgPlaza,
  ImgAlfa,
  ImgPenyetan,
  ImgRestoran,
  ImgSupplier,
  ImgAndroid,
  ImgIos,
};
